import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ClubsComponent } from './components/clubs/clubs.component';
import { SelectClubComponent } from './components/select-club/select-club.component';
import { TeamComponent } from './components/team/team.component';
import { ClickEventsComponent } from './components/click-events/click-events.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ClubsComponent,
    SelectClubComponent,
    TeamComponent,
    ClickEventsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
