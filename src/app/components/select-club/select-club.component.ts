import { Component, OnInit } from '@angular/core';
import {TeamSelect} from '../../mockClubData';
import {DutchLeague} from '../../mockClubData';


@Component({
  selector: 'app-select-club',
  templateUrl: './select-club.component.html',
  styleUrls: ['./select-club.component.css']
})
export class SelectClubComponent implements OnInit {

Team=TeamSelect;
DutchTeam=DutchLeague;

public showfirst:boolean;


  constructor() { }

  ngOnInit() {
    this.showfirst=true;
  }
public toggle(){
  console.log('shots fired');
  this.showfirst=!this.showfirst;
}

}
