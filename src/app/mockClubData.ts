import { Team } from './teams';


export const TeamSelect: Team[] = [
    { id: 1, name: 'Arsenal', url:'https://logos-download.com/wp-content/uploads/2016/05/Arsenal_logo_crest_logotype.png'
},
    { id: 2, name: 'Chelsea', url:"assets/images/Chelsea.png" },
    { id: 3, name: 'Manchester United', url:"assets/images/united.jpg" },
    { id: 4, name: 'Liverpool', url:"assets/images/liverpool.png" },
    { id: 5, name: 'West Ham',  url:"assets/images/WestHam.png"},
];

export const DutchLeague:Team[]=[

    { id: 1, name: 'Sparta', url:'assets/images/sparta.png'},
    { id: 2, name: 'Telstar', url:"assets/images/ball.png" },
    { id: 3, name: 'Ajax', url:"assets/images/Ajax.png" },
    { id: 4, name: 'Feyenoord', url:"assets/images/Fey.png" },
    { id: 5, name: 'PSV', url:"assets/images/PSV.png" },

];